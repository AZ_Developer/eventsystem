namespace EventSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VM : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Visitors",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        VName = c.String(),
                        Email = c.String(),
                        Age = c.Int(nullable: false),
                        Gender = c.String(),
                        Type = c.String(),
                        EventId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: true)
                .Index(t => t.EventId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Visitors", "EventId", "dbo.Events");
            DropIndex("dbo.Visitors", new[] { "EventId" });
            DropTable("dbo.Visitors");
        }
    }
}
