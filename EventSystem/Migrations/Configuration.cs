namespace EventSystem.Migrations
{
    using EventSystem.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<EventSystem.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(EventSystem.Models.ApplicationDbContext context)
        {
            //Add an Admin to the DB

            if (!context.Roles.Any(r => r.Name == "AppAdmin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "AppAdmin" };

                manager.Create(role);
            }
            if (!context.Roles.Any(r => r.Name == "AppEmployee"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "AppEmployee" };

                manager.Create(role);
            }
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var PasswordHash = new PasswordHasher();
            if (!context.Users.Any(u => u.UserName == "sa@admin.net"))
            {
                var user = new ApplicationUser
                {
                    UserName = "sa@admin.net",
                    Email = "sa@admin.net",
                    PasswordHash = PasswordHash.HashPassword("123456")
                };

                UserManager.Create(user);
                UserManager.AddToRole(user.Id, "AppAdmin");

            }
        }
    }
}
