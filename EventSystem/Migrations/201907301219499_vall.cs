namespace EventSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class vall : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Visitors", "VName", c => c.String(nullable: false));
            AlterColumn("dbo.Visitors", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Visitors", "Gender", c => c.String(nullable: false));
            AlterColumn("dbo.Visitors", "Type", c => c.String(nullable: false));
            AlterColumn("dbo.AspNetUsers", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.AspNetUsers", "Entity", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "Entity", c => c.String());
            AlterColumn("dbo.AspNetUsers", "Name", c => c.String());
            AlterColumn("dbo.Visitors", "Type", c => c.String());
            AlterColumn("dbo.Visitors", "Gender", c => c.String());
            AlterColumn("dbo.Visitors", "Email", c => c.String());
            AlterColumn("dbo.Visitors", "VName", c => c.String());
        }
    }
}
