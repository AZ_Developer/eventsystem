namespace EventSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addatrtomodel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Name", c => c.String());
            AddColumn("dbo.AspNetUsers", "EmployeeId", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "Entity", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Entity");
            DropColumn("dbo.AspNetUsers", "EmployeeId");
            DropColumn("dbo.AspNetUsers", "Name");
        }
    }
}
