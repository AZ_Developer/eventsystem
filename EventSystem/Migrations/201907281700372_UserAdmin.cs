namespace EventSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserAdmin : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "UserId", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "User_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.AspNetUsers", "User_Id");
            AddForeignKey("dbo.AspNetUsers", "User_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetUsers", new[] { "User_Id" });
            DropColumn("dbo.AspNetUsers", "User_Id");
            DropColumn("dbo.AspNetUsers", "UserId");
        }
    }
}
