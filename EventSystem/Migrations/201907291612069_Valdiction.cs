namespace EventSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Valdiction : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Events", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Events", "Place", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Events", "Place", c => c.String());
            AlterColumn("dbo.Events", "Name", c => c.String());
        }
    }
}
