namespace EventSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class entity : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Visitors", "Gender", c => c.String(nullable: false));
            AlterColumn("dbo.Visitors", "Type", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Visitors", "Type", c => c.String());
            AlterColumn("dbo.Visitors", "Gender", c => c.String());
        }
    }
}
