﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace EventSystem
{
    public static class Utilitarios
    {
        public static string IsLinkActive(this UrlHelper url, string action, string controller)
        {
            if (url.RequestContext.RouteData.Values["controller"].ToString() == controller &&
                url.RequestContext.RouteData.Values["action"].ToString() == action)
            {
                return "active";
            }

            return "";
        }

        public static string IsLinkActive(this UrlHelper url, string[] action, string controller)
        {
            if (url.RequestContext.RouteData.Values["controller"].ToString() == controller)
            {
                foreach (var item in action)
                {
                    if (url.RequestContext.RouteData.Values["action"].ToString() == item)
                    {
                        return "active";
                    }
                }
            }
            return "";
        }
        public static string ConvertStringArrayToString(string[] array)
        {
            //
            // Concatenate all the elements into a StringBuilder.
            //
            StringBuilder strinbuilder = new StringBuilder();
            foreach (string value in array)
            {
                strinbuilder.Append(value);
                strinbuilder.Append("','");
            }
            return strinbuilder.ToString();
        }

        public static string IsLinkActive(this UrlHelper url, string controller)
        {
            if (url.RequestContext.RouteData.Values["controller"].ToString() == controller)
            {
                return "active";
            }
            return "";
        }

        public static string IsLinkStyleActive(this UrlHelper url, string action, string controller)
        {
            if (url.RequestContext.RouteData.Values["controller"].ToString() == controller &&
                url.RequestContext.RouteData.Values["action"].ToString() == action)
            {
                return "active";
            }

            return "";
        }

        public static string IsLinkActive(this UrlHelper url, string[] controller)
        {
            foreach (var item in controller)
            {
                if (url.RequestContext.RouteData.Values["controller"].ToString() == item)
                {
                    return "active";
                }
            }
            return "";
        }

        public static string IsLinkStyleActive(this UrlHelper url, string[] controller)
        {
            foreach (var item in controller)
            {
                if (url.RequestContext.RouteData.Values["controller"].ToString() == item)
                {
                    return "active";
                }
            }
            return "";
        }
    }
}