﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EventSystem.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public Models.ApplicationUser User { get; set; }
        public int UserId { get; set; }

        [Required(ErrorMessage = "{0} مطلوب.")]
        [Display(Name = "اسم الموظف")]
        public string Name { get; set; }

        [Required(ErrorMessage = "{0} مطلوب.")]
        [Display(Name = "الرقم الوظيفي")]
        public int EmployeeId { get; set; }

        [Required(ErrorMessage = "{0} مطلوب.")]
        [Display(Name = "جهة الموظف")]
        public string Entity { get; set; }


    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }


        //The "db." it is Create by default whwn we do migrations
        public System.Data.Entity.DbSet<EventSystem.Models.Event> Events { get; set; }

        public System.Data.Entity.DbSet<EventSystem.Models.Visitor> Visitors { get; set; }

    }
}