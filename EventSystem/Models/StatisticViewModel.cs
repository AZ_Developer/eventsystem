﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventSystem.Models
{
    public class StatisticViewModel
    {
        [Display(Name = "عدد الحضور في الفعالية")]
        public int CountAll { get; set; }

        [Display(Name = "عدد الإناث")]
        public int CountFemale { get; set; }

        [Display(Name = "عدد الذكور")]
        public int CountMale { get; set; }

        [Display(Name = "عدد الطلاب")]
        public int CountStudent { get; set; }

        [Display(Name = "عدد الموظفين")]
        public int CountEmployee { get; set; }

        [Display(Name = "عدد الزائرين")]
        public int CountVisitor { get; set; }

        [Display(Name = "عدد أعضاء هيئة التدريس ")]
        public int CountStaff { get; set; }

        [Display(Name = "عدد الزائرين أقل من 18 سنه")]
        public int CountUnder18 { get; set; }

        [Display(Name = "عدد الزائرين  من 18 - 30 سنه")]
        public int Count18TO30 { get; set; }

        [Display(Name = "عدد الزائرين أكبر من 30 سنه")]
        public int CountAbove30 { get; set; }

        public Models.Event Event { get; set; }
        //public int EventId { get; set; }
    }

    public class MainViewModel
    {
        public StatisticViewModel statisticViewModel { get; set; }

        public List<Event> events { get; set; }
    }
}