﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventSystem.Models
{
    public class Event
    {
        // from here ----
        //give the list of visitor, this relation we put it in any project
        public Event()
        {
            Visitors = new HashSet<Visitor>();
            //Date = "Time of day";
        }

        // c# model one to many
        public virtual ICollection<Visitor> Visitors { get; set; }
        //TO here ----

        public int EventID { get; set; }

        [Required(ErrorMessage = "{0} مطلوب.")]
        [Display(Name = "اسم الفعالية")]
        public string Name { get; set; }

        [Required(ErrorMessage = "{0} مطلوب.")]
        [Display(Name = "مكان الفعالية")]
        public string Place { get; set; }

        [Required(ErrorMessage = "{0} مطلوب.")]
        [Display(Name = "اليوم")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }




    }
}