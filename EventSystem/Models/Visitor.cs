﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventSystem.Models
{
    public class Visitor
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "{0} مطلوب.")]
        [Display(Name = "اسم الزائر")]
        public string VName { get; set; }

        [Required(ErrorMessage = "{0} مطلوب.")]
        [Display(Name = " البريد الالكتروني")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} مطلوب.")]
        [Display(Name = " رقم الجوال")]
        public string PNumber { get; set; }

        [Required(ErrorMessage = "{0} مطلوب.")]
        [Display(Name = " العمر")]
        public int Age { get; set; }

        [Required(ErrorMessage = "{0} مطلوب.")]
        [Display(Name = " الجنس")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "{0} مطلوب.")]
        [Display(Name = " الفئة")]
        public string Type { get; set; }
        // to make connication
        public Models.Event Event { get; set; }
        public int EventId { get; set; }
    }
}