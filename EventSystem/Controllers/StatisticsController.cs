﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EventSystem.Models;

namespace EventSystem.Controllers
{
    [Authorize]
    public class StatisticsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: StatisticViewModels
        public ActionResult Index(int? id)
        {
            //the last one is for how the dropdawnlist well choese by defult
            MainViewModel mainViewModel;
            StatisticViewModel statisticView;
            var data = db.Events.ToList();

            if (id == null)
            {
                ViewBag.EventId = new SelectList(db.Events, "EventID", "Name");

                statisticView = new StatisticViewModel
                {
                    //All visitors from All Event "NO ID"
                    Event = null,
                    CountAll = db.Visitors.Count(),
                    CountMale = db.Visitors.Where(a => a.Gender == "ذكر").Count(),
                    CountFemale = db.Visitors.Where(a => a.Gender == "أنثى").Count(),
                    CountEmployee = db.Visitors.Where(a => a.Type == "موظف").Count(),
                    CountStudent = db.Visitors.Where(a => a.Type == "طالب").Count(),
                    CountStaff = db.Visitors.Where(a => a.Type == "	عضو هيئة التدريس").Count(),
                    CountVisitor = db.Visitors.Where(a => a.Type == "زائر").Count(),
                    CountUnder18 = db.Visitors.Where(a => a.Age < 18).Count(),
                    Count18TO30 = db.Visitors.Where(a => a.Age >= 18 && a.Age <= 30).Count(),
                    CountAbove30 = db.Visitors.Where(a => a.Age > 30).Count(),

                };

                mainViewModel = new MainViewModel()
                {
                    statisticViewModel = statisticView,
                    events = data
                };
            }
            else
            {

                ViewBag.EventId = new SelectList(db.Events, "EventID", "Name", id);

                var eventModel = db.Events.Find(id);
                if (eventModel == null)
                {
                    return HttpNotFound();
                }
                statisticView = new StatisticViewModel
                {
                    Event = eventModel,
                    CountAll = eventModel.Visitors.Count(),
                    CountMale = eventModel.Visitors.Where(a => a.Gender == "ذكر").Count(),
                    CountFemale = eventModel.Visitors.Where(a => a.Gender == "أنثى").Count(),
                    CountEmployee = eventModel.Visitors.Where(a => a.Type == "موظف").Count(),
                    CountStudent = eventModel.Visitors.Where(a => a.Type == "طالب").Count(),
                    CountStaff = eventModel.Visitors.Where(a => a.Type == "	عضو هيئة التدريس").Count(),
                    CountVisitor = eventModel.Visitors.Where(a => a.Type == "زائر").Count(),
                    CountUnder18 = eventModel.Visitors.Where(a => a.Age < 18).Count(),
                    Count18TO30 = eventModel.Visitors.Where(a => a.Age >= 18 && a.Age <= 30).Count(),
                    CountAbove30 = eventModel.Visitors.Where(a => a.Age > 30).Count(),

                };

            }

            mainViewModel = new MainViewModel()
            {
                statisticViewModel = statisticView,
                events = data
            };
            return View(mainViewModel);
        }
    }
}